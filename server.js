var express = require('express');
var path = require('path');
var app = express();

app.listen(process.env.PORT || 8080);
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
    res.render('home');
})

app.get('/lamborghini', function(req, res){
    res.render('lamborghini');
})

app.get('/bugatti', function(req, res){
    res.render('bugatti');
})

app.get('/wmotors', function(req, res){
    res.render('wmotors');
})

app.get('/contact', function(req, res){
    res.render('contact');
})